/**
 * Shuffled deck of cards
 *
 * Uses modern Durstenfeld algorithm of the Fisher–Yates shuffle
 * https://stackoverflow.com/a/12646864/
 *
 * @returns
 */
export function shuffledDeck() {
  const cards = [];

  for (let i = 0; i < 4; i++) {
    for (let j = 1; j <= 13; j++) {
      cards.push(`${j}-${i}`);
    }
  }

  for (let i = cards.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [cards[i], cards[j]] = [cards[j], cards[i]];
  }

  return cards;
}
