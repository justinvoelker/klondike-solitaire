import { createSlice, PayloadAction } from "@reduxjs/toolkit";

interface AutomaticCardMoveState {
  card: string | undefined;
  start: {
    x: number | undefined;
    y: number | undefined;
  };
  final: {
    x: number | undefined;
    y: number | undefined;
  };
}

const initialState: AutomaticCardMoveState = {
  card: undefined,
  start: {
    x: undefined,
    y: undefined,
  },
  final: {
    x: undefined,
    y: undefined,
  },
};

export const cardMovementSlice = createSlice({
  name: "cardMovement",
  initialState,
  reducers: {
    clearAutomaticallyMovedCard: (state) => {
      state.card = undefined;
      state.start = { x: undefined, y: undefined };
      state.final = { x: undefined, y: undefined };
    },
    setAutomaticallyMovedCard: (
      state,
      action: PayloadAction<AutomaticCardMoveState>
    ) => {
      state.card = action.payload.card;
      state.start = action.payload.start;
      state.final = action.payload.final;
    },
  },
});

export const { clearAutomaticallyMovedCard, setAutomaticallyMovedCard } =
  cardMovementSlice.actions;

export default cardMovementSlice.reducer;
