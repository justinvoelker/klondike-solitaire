import { createSlice, PayloadAction } from "@reduxjs/toolkit";

interface CardDraggingState {
  cards: Array<string>;
}

const initialState: CardDraggingState = {
  cards: [],
};

export const cardDraggingSlice = createSlice({
  name: "cardDragging",
  initialState,
  reducers: {
    clearDragging: (state) => {
      state.cards = [];
    },
    setDragging: (state, action: PayloadAction<Array<string>>) => {
      state.cards = action.payload;
    },
  },
});

export const { clearDragging, setDragging } = cardDraggingSlice.actions;

export default cardDraggingSlice.reducer;
