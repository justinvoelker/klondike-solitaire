import _ from "lodash";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";

import { shuffledDeck } from "./cardDeck";

export interface CardPile {
  cards: Array<string>;
  faceUpCount: number;
}

interface MoveCardsPayloadAction {
  cards: Array<string>;
  source: number;
  destination: number;
}

const initialStatePresent: Array<CardPile> = [
  { cards: [], faceUpCount: 0 },
  { cards: [], faceUpCount: 0 },
  { cards: [], faceUpCount: 0 },
  { cards: [], faceUpCount: 0 },
  { cards: [], faceUpCount: 0 },
  { cards: [], faceUpCount: 0 },
  { cards: [], faceUpCount: 0 },
  { cards: [], faceUpCount: 0 },
  { cards: [], faceUpCount: 0 },
  { cards: [], faceUpCount: 0 },
  { cards: [], faceUpCount: 0 },
  { cards: [], faceUpCount: 0 },
  { cards: [], faceUpCount: 0 },
];

export const gameSlice = createSlice({
  name: "game",
  initialState: {
    history: [] as Array<Array<CardPile>>,
    present: initialStatePresent,
  },
  reducers: {
    createNewGame: (state) => {
      let cards = shuffledDeck();

      // Tableau
      let startingColumn = 0;
      const tableauCards: Array<Array<string>> = [[], [], [], [], [], [], []];
      for (let i = 0; i < 7; i++) {
        const [firstCard, ...otherCards] = cards;
        tableauCards[i].push(firstCard);
        cards = otherCards;
        if (i === 6) i = startingColumn++;
        if (startingColumn > 6) break;
      }
      const tableau: Array<CardPile> = [];
      for (let i = 0; i < 7; i++) {
        tableau.push({
          cards: tableauCards[i],
          faceUpCount: 1,
        });
      }

      // Foundation
      const foundation: Array<CardPile> = [];
      for (let i = 0; i < 4; i++) {
        foundation.push({ cards: [], faceUpCount: 0 });
      }

      state.history = [];
      state.present = [
        { cards, faceUpCount: 0 },
        { cards: [], faceUpCount: 0 },
        ...foundation,
        ...tableau,
      ];
    },
    drawFromStockPile: (state, action: PayloadAction<number>) => {
      state.history = [...state.history, _.cloneDeep(state.present)];
      if (state.present[0].cards.length === 0) {
        state.present[0].cards = state.present[1].cards;
        state.present[1].cards = [];
      } else {
        state.present[1].cards = [
          ...state.present[1].cards,
          ...state.present[0].cards.slice(0, action.payload),
        ];
        state.present[0].cards = state.present[0].cards.slice(action.payload);
      }
    },
    moveCards: (state, action: PayloadAction<MoveCardsPayloadAction>) => {
      const { cards, source, destination } = action.payload;

      // Proceed with update, starting with capturing history
      state.history = [...state.history, _.cloneDeep(state.present)];

      // Remove cards from source pile
      const movedCards = state.present[source].cards.splice(
        state.present[source].cards.indexOf(cards[0]),
        state.present[source].cards.length
      );
      state.present[source].faceUpCount -= movedCards.length;

      // Append cards to destination
      state.present[destination].cards =
        state.present[destination].cards.concat(movedCards);
      state.present[destination].faceUpCount += movedCards.length;

      // Update tableau cards with no face up cards to flip last card
      for (let i = 6; i <= 12; i++) {
        if (state.present[i].faceUpCount === 0) {
          state.present[i].faceUpCount =
            state.present[i].cards.length > 0 ? 1 : 0;
        }
      }
    },
    undoLastHistory: (state) => {
      if (state.history.length > 0) {
        state.present = state.history.slice(-1)[0];
        state.history = state.history.slice(0, -1);
      }
    },
  },
});

export const { createNewGame, drawFromStockPile, moveCards, undoLastHistory } =
  gameSlice.actions;

export default gameSlice.reducer;
