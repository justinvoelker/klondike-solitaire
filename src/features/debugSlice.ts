import { createSlice, PayloadAction } from "@reduxjs/toolkit";

interface DebugState {
  enabled: boolean;
  activeDropzone: string | undefined;
}

const initialState: DebugState = {
  enabled: false,
  activeDropzone: undefined,
};

export const debugSlice = createSlice({
  name: "debug",
  initialState,
  reducers: {
    clearDropzone: (state) => {
      state.activeDropzone = undefined;
    },
    setDropzone: (state, action: PayloadAction<string>) => {
      state.activeDropzone = action.payload;
    },
    setEnabled: (state, action: PayloadAction<boolean>) => {
      state.enabled = action.payload;
    },
  },
});

export const { clearDropzone, setDropzone, setEnabled } = debugSlice.actions;

export default debugSlice.reducer;
