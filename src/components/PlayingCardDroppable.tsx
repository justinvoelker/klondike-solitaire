import { Data, useDroppable } from "@dnd-kit/core";
import { useAppSelector } from "../app/hooks";

interface DroppableProps {
  id: string;
  data?: Data;
}

export function PlayingCardDroppable({ id, data }: DroppableProps) {
  const { activeDropzone, enabled: isDebugging } = useAppSelector(
    (state) => state.debug
  );
  const { setNodeRef } = useDroppable({ id, data });

  let className = "aspect-[2.5/3.5] w-12 sm:w-20";
  if (isDebugging) {
    className = `${className} border-2 sm:border-4 ${
      activeDropzone?.toString() === id ? "border-red-800" : "border-yellow-400"
    }`;
  }

  return <div ref={setNodeRef} className={className}></div>;
}
