import { HeartIcon, DiamondIcon, SpadeIcon, ClubIcon } from "./Icons";

interface PlayingCardProps {
  card: string;
  faceUp?: boolean;
}

export function PlayingCard({ card, faceUp = true }: PlayingCardProps) {
  const [rank, suit] = card.split("-");

  const classNameColor =
    parseInt(suit) <= 1
      ? " fill-red-600 text-red-600"
      : " fill-gray-600 text-gray-600";

  return (
    <div className="relative aspect-[2.5/3.5] w-12 sm:w-20">
      <div
        className={`absolute aspect-[2.5/3.5] w-12 select-none rounded-md border-2 border-gray-300 bg-white sm:w-20 sm:border-4 ${classNameColor}`}
        style={{
          backfaceVisibility: "hidden",
          transform: faceUp ? "rotateY(0deg)" : "rotateY(180deg)",
          transition: "transform 200ms ease",
        }}
      >
        <div className="absolute top-0.5 left-1 w-1/3 text-center text-sm font-extrabold leading-4 sm:text-2xl  sm:leading-6">
          {rankCharacter({ rank })}
        </div>
        <div className="absolute top-1 right-1 w-1/3 ">
          <div className="mx-auto my-0.5 w-3 sm:w-5">{suitIcon({ suit })}</div>
        </div>
        <div className="absolute bottom-0 w-full">
          <div className="my-0 mx-auto w-10 sm:w-16 ">{suitIcon({ suit })}</div>
        </div>
      </div>
      <div
        className="absolute aspect-[2.5/3.5] w-12 rounded-md border-2 border-cyan-600 bg-cyan-700 sm:w-20 sm:border-4"
        style={{
          backfaceVisibility: "hidden",
          transform: faceUp ? "rotateY(180deg)" : "rotateY(0deg)",
          transition: "transform 200ms ease",
        }}
      ></div>
    </div>
  );
}

function rankCharacter({ rank }: { rank: string }) {
  if (rank === "1") return "A";
  if (rank === "11") return "J";
  if (rank === "12") return "Q";
  if (rank === "13") return "K";
  return rank.toString();
}

function suitIcon({ suit }: { suit: string }) {
  if (suit === "0") return <HeartIcon />;
  if (suit === "1") return <DiamondIcon />;
  if (suit === "2") return <SpadeIcon />;
  if (suit === "3") return <ClubIcon />;
  return <></>;
}
