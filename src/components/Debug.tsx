import { useEffect } from "react";
import { useAppDispatch, useAppSelector } from "../app/hooks";
import { setEnabled } from "../features/debugSlice";

interface DebugProps {
  enabled?: boolean;
}

export function Debug({ enabled = false }: DebugProps) {
  const debug = useAppSelector((state) => state.debug);
  const cardDragging = useAppSelector((state) => state.cardDragging);
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(setEnabled(enabled));
  }, [dispatch, enabled]);

  if (!enabled) return <></>;

  return (
    <div className="absolute bottom-0 left-0">
      <div>Active dropzone: {debug.activeDropzone}</div>
      <div>Dragging cards: {cardDragging.cards.join(", ")}</div>
    </div>
  );
}
