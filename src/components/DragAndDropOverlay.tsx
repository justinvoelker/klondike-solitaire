import { DragOverlay } from "@dnd-kit/core";

import { useAppSelector } from "../app/hooks";
import { PlayingCardPileStatic } from "./PlayingCardPileStatic";

interface DragAndDropOverlayProps {
  easeDuration: number;
}

export function DragAndDropOverlay({ easeDuration }: DragAndDropOverlayProps) {
  const cardDragging = useAppSelector((state) => state.cardDragging);

  return (
    <DragOverlay dropAnimation={{ duration: easeDuration, easing: "ease" }}>
      {cardDragging.cards && (
        <PlayingCardPileStatic
          cards={cardDragging.cards}
          pileLayout="auto-rows-[minmax(0,_1.5rem)] sm:auto-rows-[minmax(0,_2rem)]"
        />
      )}
    </DragOverlay>
  );
}
