import { useEffect, useState } from "react";

import { useAppSelector } from "../app/hooks";
import { Draggable } from "./Draggable";
import { PlayingCardDroppable } from "./PlayingCardDroppable";
import { PlayingCard } from "./PlayingCard";
import { PlayingCardDoubleClickable } from "./PlayingCardDoubleClickable";

export interface PlayingCardPileProps {
  cards: Array<string>;
  draggableCount: number;
  faceUpCount: number;
  pileId: number;
  pileLayout?: string;
}

export function PlayingCardPile({
  cards,
  draggableCount,
  faceUpCount,
  pileId,
  pileLayout = "auto-rows-[minmax(0,_0rem)]",
}: PlayingCardPileProps) {
  const cardDragging = useAppSelector((state) => state.cardDragging);
  const [activeDragCards, setActiveDragCards] = useState<Array<string>>([]);

  const cardPile = [];

  // Using "cardDragging.cards" directly in the opacity conditional resulted in a flicker where the opacity of 0 was applied to the card pile before the drag overlay could render its replacement card stack. I think this is just an example of me not understanding the render process of React and when hooks are evaluated.
  useEffect(() => {
    setActiveDragCards(cardDragging.cards);
  }, [cardDragging]);

  if (cards) {
    for (let i = 0; i < cards.length; i++) {
      const card = cards[i];
      cardPile.push(
        <div key={card} id={card} className="aspect-[2.5/3.5] w-12 sm:w-20">
          <Draggable
            id={card}
            disabled={i < cards.length - draggableCount}
            data={{
              cards: cards.slice(i, i + draggableCount),
              source: pileId,
            }}
          >
            <div
              className={`${activeDragCards.includes(card) ? "opacity-0" : ""}`}
            >
              <PlayingCardDoubleClickable
                card={card}
                enabled={i >= cards.length - faceUpCount}
              >
                <PlayingCard
                  card={card}
                  faceUp={i >= cards.length - faceUpCount}
                />
              </PlayingCardDoubleClickable>
            </div>
          </Draggable>
        </div>
      );
    }
    cardPile.push(
      <PlayingCardDroppable
        key={pileId}
        id={pileId.toString()}
        data={{ destination: pileId }}
      />
    );
  }

  return (
    <div id={`pile-${pileId}`} className="relative">
      <div className="aspect-[2.5/3.5] w-12 rounded-md border-2 border-white/20 sm:w-20 sm:border-4" />
      <div className={`absolute top-0 left-0 grid ${pileLayout}`}>
        {cardPile}
      </div>
    </div>
  );
}
