import { Data, useDraggable } from "@dnd-kit/core";

interface DraggableProps {
  children: React.ReactNode;
  id: string;
  data?: Data;
  disabled?: boolean;
}

export function Draggable({ children, id, data, disabled }: DraggableProps) {
  const { attributes, isDragging, listeners, setNodeRef, transform } =
    useDraggable({
      id,
      data,
      disabled,
      attributes: { tabIndex: disabled ? -1 : 0 },
    });

  const style = {
    transform: transform
      ? `translate3d(${transform.x}px, ${transform.y}px, 0)`
      : undefined,
    zIndex: isDragging ? "1" : undefined,
    position: isDragging ? ("relative" as const) : undefined,
    cursor: disabled ? "auto" : undefined,
  };
  return (
    <div ref={setNodeRef} style={style} {...listeners} {...attributes}>
      {children}
    </div>
  );
}
