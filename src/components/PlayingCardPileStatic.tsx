import { PlayingCard } from "./PlayingCard";

export interface PlayingCardPileProps {
  cards: Array<string>;
  pileLayout?: string;
}

export function PlayingCardPileStatic({
  cards,
  pileLayout = "auto-rows-[minmax(0,_0rem)]",
}: PlayingCardPileProps) {
  const cardPile = [];

  if (cards) {
    for (let i = 0; i < cards.length; i++) {
      const card = cards[i];
      cardPile.push(
        <div key={card} className="aspect-[2.5/3.5] w-12 sm:w-20">
          <PlayingCard card={card} />
        </div>
      );
    }
  }

  return <div className={`grid ${pileLayout}`}>{cardPile}</div>;
}
