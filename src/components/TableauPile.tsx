import { useAppSelector } from "../app/hooks";
import { PlayingCardPile } from "./PlayingCardPile";

interface TableauPileProps {
  index: number;
}

export function TableauPile({ index }: TableauPileProps) {
  const { cards, faceUpCount } = useAppSelector(
    (state) => state.game.present[index]
  );

  return (
    <PlayingCardPile
      pileId={index}
      cards={cards}
      draggableCount={faceUpCount}
      faceUpCount={faceUpCount}
      pileLayout="auto-rows-[minmax(0,_1.5rem)] sm:auto-rows-[minmax(0,_2rem)]"
    />
  );
}
