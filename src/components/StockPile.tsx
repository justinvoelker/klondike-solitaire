import _ from "lodash";

import { useAppDispatch, useAppSelector } from "../app/hooks";
import { drawFromStockPile } from "../features/gameSlice";
import { PlayingCardPile } from "./PlayingCardPile";

export function StockPile() {
  const { cards } = useAppSelector((state) => state.game.present[0]);

  const dispatch = useAppDispatch();

  return (
    <div
      onClick={() => dispatch(drawFromStockPile(3))}
      onKeyPress={() => dispatch(drawFromStockPile(3))}
      role="button"
      tabIndex={0}
    >
      <PlayingCardPile
        pileId={0}
        cards={_.cloneDeep(cards).reverse()}
        draggableCount={0}
        faceUpCount={0}
      />
    </div>
  );
}
