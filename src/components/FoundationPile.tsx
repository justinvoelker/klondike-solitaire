import { useAppSelector } from "../app/hooks";
import { PlayingCardPile } from "./PlayingCardPile";

interface FoundationPileProps {
  index: number;
}

export function FoundationPile({ index }: FoundationPileProps) {
  const { cards, faceUpCount } = useAppSelector(
    (state) => state.game.present[index]
  );

  return (
    <PlayingCardPile
      pileId={index}
      cards={cards}
      draggableCount={1}
      faceUpCount={faceUpCount}
    />
  );
}
