import React, { CSSProperties, useEffect, useState } from "react";
import { useAppDispatch, useAppSelector } from "../app/hooks";
import { clearAutomaticallyMovedCard } from "../features/cardMovementSlice";
import { useMoveCardToFoundation } from "../hooks/useMoveCardToFoundation";
import { PlayingCardPileStatic } from "./PlayingCardPileStatic";

interface handleClickProps {
  event: React.MouseEvent<HTMLElement>;
  card: string;
}

interface handleKeyPressProps {
  event: React.KeyboardEvent<HTMLElement>;
  card: string;
}

interface PlayingCardDoubleClickableProps {
  children: React.ReactNode;
  card: string;
  enabled: boolean;
}

const animateDuration = 200;

export function PlayingCardDoubleClickable({
  children,
  card,
  enabled,
}: PlayingCardDoubleClickableProps) {
  const dispatch = useAppDispatch();

  const { moveCardToFoundation } = useMoveCardToFoundation();
  const cardMovement = useAppSelector((state) => state.cardMovement);

  const [moveInProgress, setMoveInProgress] = useState<boolean>(false);
  const [cardStyle, setCardStyle] = useState<CSSProperties>();
  const [fauxStyle, setFauxStyle] = useState<CSSProperties>();

  if (cardMovement.card === card && !moveInProgress) {
    setCardStyle({ opacity: 0 });
    setFauxStyle({
      position: "fixed",
      transition: `all ${animateDuration}ms ease`,
      left: `${cardMovement.start.x}px`,
      top: `${cardMovement.start.y}px`,
      zIndex: 1,
    });
    setMoveInProgress(true);
  }

  useEffect(() => {
    if (cardMovement.card === card && moveInProgress) {
      setFauxStyle({
        position: "fixed",
        transition: `all ${animateDuration}ms ease`,
        left: `${cardMovement.final.x}px`,
        top: `${cardMovement.final.y}px`,
        zIndex: 1,
      });
      dispatch(clearAutomaticallyMovedCard());
      setTimeout(() => {
        setMoveInProgress(false);
        setCardStyle({ opacity: 1 });
        setFauxStyle({});
      }, animateDuration);
    }
  }, [card, cardMovement, dispatch, moveInProgress]);

  function handleClick({ event, card }: handleClickProps) {
    if (event.detail === 2) {
      moveCardToFoundation({ card });
    }
  }
  function handleKeyPress({ event, card }: handleKeyPressProps) {
    // The onKeyPress event is being swallowed by Draggable
    console.log(JSON.stringify(event), card);
  }

  // Negative tab index to prevent double focus (since Draggable already captures focus once)
  // A custom Draggable/DoubleClickable component should probably combine these...
  return (
    <>
      {moveInProgress && (
        <div id="faux" style={fauxStyle}>
          <PlayingCardPileStatic cards={[card]} />
        </div>
      )}
      {enabled ? (
        <div
          style={cardStyle}
          role="button"
          tabIndex={-1}
          onClick={(event) => handleClick({ event, card })}
          onKeyPress={(event) => handleKeyPress({ event, card })}
        >
          {children}
        </div>
      ) : (
        <div style={cardStyle}>{children}</div>
      )}
    </>
  );
}
