import { useEffect } from "react";
import Confetti from "react-confetti";

import { useAppDispatch } from "../app/hooks";
import { createNewGame, undoLastHistory } from "../features/gameSlice";
import { useGameStatus } from "../hooks/useGameStatus";
import { useMoveWinningCards } from "../hooks/useMoveWinningCards";
import { Debug } from "./Debug";
import { DragAndDropContext } from "./DragAndDropContext";
import { DragAndDropOverlay } from "./DragAndDropOverlay";
import { FoundationPile } from "./FoundationPile";
import { StockPile } from "./StockPile";
import { TableauPile } from "./TableauPile";
import { WastePile } from "./WastePile";

const easeDuration = 200;

export function Game() {
  const dispatch = useAppDispatch();

  const { isComplete, isWinnable } = useGameStatus();
  const { moveWinningCards } = useMoveWinningCards();

  function handleCreateNewGame() {
    dispatch(createNewGame());
  }

  useEffect(() => {
    dispatch(createNewGame());
  }, [dispatch]);

  useEffect(() => {
    if (isWinnable) {
      moveWinningCards();
    }
  }, [isWinnable, moveWinningCards]);

  const foundationItems = [];
  for (let i = 2; i < 6; i++) {
    foundationItems.push(<FoundationPile key={`pile-${i}`} index={i} />);
  }

  const tableauItems = [];
  for (let i = 6; i <= 12; i++) {
    tableauItems.push(<TableauPile key={`pile-${i}`} index={i} />);
  }

  return (
    <DragAndDropContext easeDuration={easeDuration}>
      <div className="h-full w-full overflow-hidden bg-green-700">
        <div className="w-full bg-black/20">
          <div className="mx-auto grid w-full max-w-2xl grid-cols-7 justify-items-center gap-1 p-1 sm:gap-2 sm:p-2">
            <StockPile />
            <WastePile />
            <div className="flex w-full flex-col gap-1 sm:gap-2">
              <button
                className="h-6 rounded-md bg-black/20 text-xs font-bold text-white/20 sm:h-8 sm:text-base"
                onClick={handleCreateNewGame}
              >
                NEW
              </button>
              <button
                className="h-6 grow rounded-md bg-black/20 text-xs font-bold text-white/20 sm:h-8 md:text-base"
                onClick={() => dispatch(undoLastHistory())}
              >
                UNDO
              </button>
            </div>
            {foundationItems}
          </div>
        </div>
        <div className="h-full w-full">
          <div className="mx-auto grid h-full w-full max-w-2xl grid-cols-7 justify-items-center gap-1 p-1 sm:gap-2 sm:p-2">
            {tableauItems}
          </div>
        </div>
      </div>
      <DragAndDropOverlay easeDuration={easeDuration} />
      {isComplete && <Confetti />}
      <Debug enabled={false} />
    </DragAndDropContext>
  );
}
