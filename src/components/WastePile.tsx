import { useAppSelector } from "../app/hooks";
import { PlayingCardPile } from "./PlayingCardPile";

export function WastePile() {
  const { cards } = useAppSelector((state) => state.game.present[1]);

  return (
    <PlayingCardPile
      pileId={1}
      cards={cards}
      draggableCount={1}
      faceUpCount={cards.length}
    />
  );
}
