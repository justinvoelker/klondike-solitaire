import {
  ClientRect,
  CollisionDescriptor,
  CollisionDetection,
  DndContext,
  DragCancelEvent,
  DragEndEvent,
  DragOverEvent,
  DragStartEvent,
  KeyboardSensor,
  PointerSensor,
  useSensor,
  useSensors,
} from "@dnd-kit/core";
import { Coordinates } from "@dnd-kit/utilities";

import { useAppDispatch } from "../app/hooks";
import { clearDragging, setDragging } from "../features/cardDraggingSlice";
import { clearDropzone, setDropzone } from "../features/debugSlice";
import { moveCards } from "../features/gameSlice";
import { useIsValidMove } from "../hooks/useIsValidMove";

interface DragAndDropContextProps {
  children: React.ReactNode;
  easeDuration: number;
}

export function DragAndDropContext({
  children,
  easeDuration,
}: DragAndDropContextProps) {
  const dispatch = useAppDispatch();
  const { isValidMove } = useIsValidMove();

  function handleDragStart(event: DragStartEvent) {
    if (event.active.data.current) {
      dispatch(setDragging(event.active.data.current.cards));
    }
  }

  function handleDragEnd(event: DragEndEvent) {
    setTimeout(() => {
      dispatch(clearDragging());
      dispatch(clearDropzone());
    }, easeDuration);

    if (event.over?.data.current && event.active.data.current) {
      const isValid = isValidMove({
        cards: event.active.data.current.cards,
        source: event.active.data.current.source,
        destination: event.over.data.current.destination,
      });

      if (isValid) {
        dispatch(
          moveCards({
            cards: event.active.data.current.cards,
            source: event.active.data.current.source,
            destination: event.over.data.current.destination,
          })
        );
      }
    }
  }

  function handleDragCancel(event: DragCancelEvent) {
    if (event.active.data.current) {
      setTimeout(() => {
        dispatch(clearDragging());
        dispatch(clearDropzone());
      }, easeDuration);
    }
  }

  function handleDragOver(event: DragOverEvent) {
    if (event.over?.data.current) {
      dispatch(setDropzone(event.over.data.current.destination));
    }
  }

  // Allow double clicking without @dnd-kit completely capturing all click events
  const dndSensors = useSensors(
    useSensor(PointerSensor, {
      activationConstraint: { distance: 2 },
    }),
    useSensor(KeyboardSensor)
  );

  return (
    <DndContext
      onDragStart={handleDragStart}
      onDragEnd={handleDragEnd}
      onDragCancel={handleDragCancel}
      onDragOver={handleDragOver}
      sensors={dndSensors}
      collisionDetection={closestCard}
    >
      {children}
    </DndContext>
  );
}

function centerOfRectangle(
  rect: ClientRect,
  left = rect.left,
  top = rect.top
): Coordinates {
  return {
    x: left + rect.width * 0.5,
    y: top + rect.width * (3.5 / 2.5) * 0.5,
  };
}

export const closestCard: CollisionDetection = ({
  collisionRect,
  droppableRects,
  droppableContainers,
}) => {
  const centerRect = centerOfRectangle(
    collisionRect,
    collisionRect.left,
    collisionRect.top
  );
  const collisions: CollisionDescriptor[] = [];

  for (const droppableContainer of droppableContainers) {
    const { id } = droppableContainer;
    const rect = droppableRects.get(id);

    if (rect) {
      const distBetween = distanceBetween(centerOfRectangle(rect), centerRect);

      collisions.push({ id, data: { droppableContainer, value: distBetween } });
    }
  }

  return collisions.sort(sortCollisionsAsc);
};

export function sortCollisionsAsc(
  { data: { value: a } }: CollisionDescriptor,
  { data: { value: b } }: CollisionDescriptor
) {
  return a - b;
}

export function distanceBetween(p1: Coordinates, p2: Coordinates) {
  return Math.sqrt(Math.pow(p1.x - p2.x, 2) + Math.pow(p1.y - p2.y, 2));
}
