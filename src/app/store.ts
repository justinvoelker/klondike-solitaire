import { configureStore } from "@reduxjs/toolkit";
import debugReducer from "../features/debugSlice";
import gameReducer from "../features/gameSlice";
import cardDraggingReducer from "../features/cardDraggingSlice";
import cardMovementReducer from "../features/cardMovementSlice";

export const store = configureStore({
  reducer: {
    debug: debugReducer,
    game: gameReducer,
    cardDragging: cardDraggingReducer,
    cardMovement: cardMovementReducer,
  },
});

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>;
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch;
