import { useCallback } from "react";
import { useAppSelector } from "../app/hooks";
import { useMoveCardToFoundation } from "./useMoveCardToFoundation";

export function useMoveWinningCards() {
  const piles = useAppSelector((state) => state.game.present);
  const { moveCardToFoundation } = useMoveCardToFoundation();

  const moveWinningCards = useCallback(() => {
    const tableauTotal = piles
      .slice(6)
      .reduce((pV, cV) => pV + cV.cards.length, 0);

    const cardsRemaining: Array<string> = [];
    if (tableauTotal > 0) {
      for (let rank = 1; rank <= 13; rank++) {
        for (let suit = 0; suit <= 3; suit++) {
          const card = `${rank}-${suit}`;
          for (let i = 6; i <= 12; i++) {
            if (piles[i].cards.includes(card)) {
              cardsRemaining.push(card);
            }
          }
        }
      }
    }

    setTimeout(() => {
      if (cardsRemaining[0]) {
        moveCardToFoundation({ card: cardsRemaining[0] });
      }
    }, 50);
  }, [moveCardToFoundation, piles]);

  return { moveWinningCards };
}
