import { useEffect, useState } from "react";
import { useAppSelector } from "../app/hooks";

export function useGameStatus() {
  const piles = useAppSelector((state) => state.game.present);
  const [isComplete, setIsComplete] = useState(false);
  const [isWinnable, setIsWinnable] = useState(false);

  useEffect(() => {
    const foundationTotal = piles
      .slice(2, 6)
      .reduce((pV, cV) => pV + cV.cards.length, 0);
    const tableauTotal = piles
      .slice(6)
      .reduce((pV, cV) => pV + cV.cards.length, 0);
    const tableauFaceUp = piles
      .slice(6)
      .reduce((pV, cV) => pV + cV.faceUpCount, 0);

    setIsComplete(foundationTotal === 52);

    setIsWinnable(
      piles[0].cards.length === 0 &&
        piles[1].cards.length === 0 &&
        tableauTotal > 0 &&
        tableauTotal === tableauFaceUp
    );
  }, [piles]);

  return { isComplete, isWinnable };
}
