import { useCallback } from "react";
import { useCalculateFoundationAccepts } from "./useCalculateFoundationAccepts";
import { useCalculateTableauAccepts } from "./useCalculateTableauAccepts";

interface IsValidMoveProps {
  cards: Array<string>;
  source: number;
  destination: number;
}

export function useIsValidMove() {
  const { calculateAccepts: calculateTableauAccepts } =
    useCalculateTableauAccepts();
  const { calculateAccepts: calculateFoundationAccepts } =
    useCalculateFoundationAccepts();

  const isValidMove = useCallback(
    ({ cards, source, destination }: IsValidMoveProps) => {
      // False if source matches destination
      if (source === destination) return false;

      // False if more than one card moved to foundation
      if (destination >= 2 && destination <= 5 && cards.length > 1)
        return false;

      // False if destination does not accept first card
      let accepts: Array<string> = [];
      if (destination >= 6 && destination <= 12) {
        accepts = calculateTableauAccepts(destination);
      } else if (destination >= 2 && destination <= 5) {
        accepts = calculateFoundationAccepts(destination);
      }
      if (!accepts.includes(cards[0])) return false;

      // All else, true
      return true;
    },
    [calculateFoundationAccepts, calculateTableauAccepts]
  );

  return { isValidMove };
}
