import { useCallback } from "react";

import { useAppSelector } from "../app/hooks";

export function useCalculateTableauAccepts() {
  const piles = useAppSelector((state) => state.game.present);

  const calculateAccepts = useCallback(
    (index: number) => {
      const cards = piles[index].cards;
      if (!cards.length) return ["13-0", "13-1", "13-2", "13-3"];
      const [rank, suit] = cards[cards.length - 1].split("-");
      const nextRank = parseInt(rank) - 1;
      if (nextRank === 0) return [];
      if (parseInt(suit) > 1) return [`${nextRank}-0`, `${nextRank}-1`];
      return [`${nextRank}-2`, `${nextRank}-3`];
    },
    [piles]
  );

  return { calculateAccepts };
}
