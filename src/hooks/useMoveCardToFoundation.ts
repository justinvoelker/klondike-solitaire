import { useCallback } from "react";
import { useAppDispatch, useAppSelector } from "../app/hooks";
import { setAutomaticallyMovedCard } from "../features/cardMovementSlice";
import { moveCards, CardPile } from "../features/gameSlice";
import { useCalculateFoundationAccepts } from "./useCalculateFoundationAccepts";

export function useMoveCardToFoundation() {
  const dispatch = useAppDispatch();
  const { calculateAccepts } = useCalculateFoundationAccepts();
  const piles = useAppSelector((state) => state.game.present);

  const moveCardToFoundation = useCallback(
    ({ card }: { card: string }) => {
      // Two empty arrays account for the stock and waste piles--foundation starts at index 2
      const foundationsAccept: Array<Array<string>> = [[], []];
      for (let i = 2; i <= 5; i++) {
        foundationsAccept.push(calculateAccepts(i));
      }

      const { src, dst } = calculateMoveToFoundation({
        foundationsAccept,
        card,
        piles,
      });

      if (src.pile >= 0 && dst.pile >= 0) {
        dispatch(
          moveCards({ cards: [card], source: src.pile, destination: dst.pile })
        );
        dispatch(
          setAutomaticallyMovedCard({
            card,
            start: { x: src.x, y: src.y },
            final: { x: dst.x, y: dst.y },
          })
        );
      }
    },
    [calculateAccepts, dispatch, piles]
  );

  return { moveCardToFoundation };
}

interface calculateMoveProps {
  foundationsAccept: Array<Array<string>>;
  card: string;
  piles: Array<CardPile>;
}

function calculateMoveToFoundation({
  foundationsAccept,
  card,
  piles,
}: calculateMoveProps) {
  const source = piles.findIndex(
    (pile) => pile.cards[pile.cards.length - 1] === card
  );
  const destination = foundationsAccept.findIndex((pile) =>
    pile.includes(card)
  );

  let srcX;
  let srcY;
  if (source >= 0) {
    const { x, y } = document
      .getElementById(card)
      ?.getBoundingClientRect() as DOMRect;
    srcX = x;
    srcY = y;
  }

  let dstX;
  let dstY;
  if (destination >= 0) {
    const { x, y } = document
      .getElementById(`pile-${destination}`)
      ?.getBoundingClientRect() as DOMRect;
    dstX = x;
    dstY = y;
  }

  return {
    src: { pile: source, x: srcX, y: srcY },
    dst: { pile: destination, x: dstX, y: dstY },
  };
}
