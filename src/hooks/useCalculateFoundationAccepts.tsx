import { useCallback } from "react";

import { useAppSelector } from "../app/hooks";

export function useCalculateFoundationAccepts() {
  const piles = useAppSelector((state) => state.game.present);

  const calculateAccepts = useCallback(
    (index: number) => {
      const cards = piles[index].cards;
      if (!cards.length) return ["1-0", "1-1", "1-2", "1-3"];
      const [rank, suit] = cards[cards.length - 1].split("-");
      if (parseInt(rank) <= 13) return [`${parseInt(rank) + 1}-${suit}`];
      return [];
    },
    [piles]
  );

  return { calculateAccepts };
}
